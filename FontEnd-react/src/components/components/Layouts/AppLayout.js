import { useAuth } from '../../../api/auth'

const AppLayout = ({ children }) => {
  const { user } = useAuth({ middleware: 'auth' })
   
  return (
    <div className="min-h-screen">
      <main>{children}</main>
    </div>
  )
}

export default AppLayout
