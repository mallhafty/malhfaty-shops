import { Fragment, useState } from 'react'
import { Dialog, Transition } from '@headlessui/react'
import { XMarkIcon } from '@heroicons/react/24/outline'

// Redux
import { useDispatch, useSelector } from 'react-redux'
import { decrementProductQuantity, incrementProductQuantity, removeFromCart } from '../../redux/actions'



export default function Card() {
  const myStyle = {
    display: "-webkit-box",
    WebkitLineClamp: 1,
    WebkitBoxOrient: "vertical",
    overflow: "hidden"
  };
  
  const products = useSelector(state => state.cart)
  const totalPrice = useSelector(state => state.totalPrice).toFixed()
  
  const dispatch = useDispatch();

  // const handleClick = () => {
  //     dispatch(incrementProductQuantity(products.id));
  // }
  
  const [open, setOpen] = useState(false)

  return (
    <div>
      <div className="relative scale-75" onClick={() => setOpen(true)}>
        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor"  className="h-8 w-8 text-white" viewBox="0 0 16 16">
          <path d="M8 1a2 2 0 0 0-2 2v2H5V3a3 3 0 1 1 6 0v2h-1V3a2 2 0 0 0-2-2zM5 5H3.36a1.5 1.5 0 0 0-1.483 1.277L.85 13.13A2.5 2.5 0 0 0 3.322 16h9.355a2.5 2.5 0 0 0 2.473-2.87l-1.028-6.853A1.5 1.5 0 0 0 12.64 5H11v1.5a.5.5 0 0 1-1 0V5H6v1.5a.5.5 0 0 1-1 0V5z" />
        </svg>
        {products.length? <span className="absolute -top-1.5 right-0 rounded-full bg-blue-600w p-1.5 text-sm text-white"></span> : null}
      </div>
      <Transition.Root show={open} as={Fragment} className="z-50">
        <Dialog as="div" className="relative z-10" onClose={setOpen}>
          <Transition.Child
            as={Fragment}
            enter="ease-in-out duration-500"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in-out duration-500"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-gray-500 bg-opacity-20 transition-opacity" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-hidden">
            <div className="absolute inset-0 overflow-hidden">
              <div className="pointer-events-none fixed inset-y-0 right-0 flex max-w-full pl-10">
                <Transition.Child
                  as={Fragment}
                  enter="transform transition ease-in-out duration-500 sm:duration-700"
                  enterFrom="translate-x-full"
                  enterTo="translate-x-0"
                  leave="transform transition ease-in-out duration-500 sm:duration-700"
                  leaveFrom="translate-x-0"
                  leaveTo="translate-x-full"
                >
                  <Dialog.Panel className="pointer-events-auto w-screen max-w-md">
                    <div className="flex h-full flex-col overflow-y-scroll bg-white shadow-xl">
                      <div className="flex-1 overflow-y-auto px-4 py-6 sm:px-6">
                        <div className="flex items-start justify-between">
                          <Dialog.Title className="text-lg font-medium text-gray-900">Shopping cart</Dialog.Title>
                          <div className="ml-3 flex h-7 items-center">
                            <button
                              type="button"
                              className="-m-2 p-2 text-gray-400 hover:text-gray-500"
                              onClick={() => setOpen(false)}
                            >
                              <span className="sr-only">Close panel</span>
                              <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                            </button>
                          </div>
                        </div>

                        <div className="mt-8">
                          <div className="flow-root">
                            <ul role="list" className="-my-6 divide-y divide-gray-200">
                              {products.map((product) => (
                                <li key={product.id} className="flex py-6">
                                  <div className="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                                    <img
                                      src={`http://localhost:8000/images/${product.image}`}
                                      alt={product.title}
                                      className="h-full w-full object-cover object-center"
                                    />
                                  </div>

                                  <div className="ml-4 flex flex-1 flex-col">
                                    <div>
                                      <div className="flex justify-between text-base font-medium text-gray-900">
                                        <h3>
                                          <a style={myStyle}>{product.title}</a>
                                        </h3>
                                        <div className="ml-4 flex space-x-1"><p>{product.price}</p><p> $</p></div>
                                      </div>
                                      <p className="mt-1 text-sm text-gray-500">total : {(product.qty * product.price).toFixed()} $</p>
                                    </div>
                                    <div className="flex flex-1 items-end justify-between text-sm">
                                      <p className="text-gray-500">Qty {product.qty}</p>

                                      <div className="flex gap-1">
                                        <button onClick={()=>dispatch(incrementProductQuantity(product.id))} type="button" className="w-7 border p-1 font-medium text-blue-600" >
                                          +
                                        </button>
                                        <button onClick={()=>dispatch(decrementProductQuantity(product.id))} type="button" className="w-7 border p-1 font-medium text-blue-600" >
                                          -
                                        </button>
                                        <button onClick={()=>dispatch(removeFromCart(product.id))} type="button" className="w-7 border p-1 font-medium text-blue-600" >
                                          x
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                              ))}
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div className="border-t border-gray-200 px-4 py-6 sm:px-6">
                        <div className="flex justify-between text-base font-medium text-gray-900">
                          <p>Subtotal</p>
                          <p>{totalPrice} $</p>
                        </div>
                        <div className="mt-6">
                          <a
                            href="#"
                            className="flex items-center justify-center rounded-md border border-transparent bg-blue-600 px-6 py-3 text-base font-medium text-white shadow-sm "
                          >
                            Checkout
                          </a>
                        </div>
                      </div>
                    </div>
                  </Dialog.Panel>
                </Transition.Child>
              </div>
            </div>
          </div>
        </Dialog>
      </Transition.Root>
    </div>
  )
}
