import { Link, NavLink } from "react-router-dom";
import { useState } from "react";
import { useAuth } from '../../api/auth'
import { DropdownButton } from "../components/DropdownLink";
import Dropdown from "../components/Dropdown";

const Navbar = () => {
  const { user } = useAuth({ middleware: 'guest' })
  const { logout } = useAuth()
  const [isOpen, setIsOpen] = useState(false);
  const toggleNavbar = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <header className="fixed flex flex-wrap sm:justify-start sm:flex-nowrap z-50 w-full  border-b border-gray-200 text-sm py-3 sm:py-0 bg-white dark:border-gray-700">
        <nav className="relative max-w-7xl w-full mx-auto px-4 sm:flex sm:items-center sm:justify-between sm:px-6 lg:px-8" aria-label="Global">
          <div className="flex items-center justify-between">
            <Link to="/" className="flex-shrink-0">
              <span className="text-blue-600 text-2xl font-bold  ">Mall<span className=" text-gray-900 ">Hafty</span></span>
            </Link>
            <div className="sm:hidden">
              <button type="button" onClick={toggleNavbar} className="hs-collapse-toggle p-2 inline-flex justify-center items-center gap-2 rounded-md border font-medium bg-white text-gray-700 shadow-sm align-middle hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-white focus:ring-blue-600 transition-all text-sm dark:bg-slate-900 dark:hover:bg-slate-800 dark:border-gray-700 dark:text-gray-400 dark:hover:text-white dark:focus:ring-offset-gray-800" data-hs-collapse="#navbar-collapse-with-animation" aria-controls="navbar-collapse-with-animation" aria-label="Toggle navigation">
                <svg className="hs-collapse-open:hidden w-4 h-4" width={16} height={16} fill="currentColor" viewBox="0 0 16 16">
                  <path fillRule="evenodd" d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z" />
                </svg>
                <svg className="hs-collapse-open:block hidden w-4 h-4" width={16} height={16} fill="currentColor" viewBox="0 0 16 16">
                  <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                </svg>
              </button>
            </div>
          </div>
          <div id="navbar-collapse-with-animation" className={`hs-collapse ${isOpen ? 'block' : 'hidden'} transition-all duration-300 basis-full grow sm:block`}>
            <div className="flex flex-col gap-y-4 gap-x-0 mt-5 sm:flex-row sm:items-center sm:justify-end sm:gap-y-0 sm:gap-x-7 sm:mt-0 sm:pl-7">
              <Link to={"/"} className="font-medium text-gray-500 hover:text-gray-400 sm:py-6 dark:text-gray-400 dark:hover:text-gray-500" >Home</Link>
              <Link to={'/products'} className="font-medium text-gray-500 hover:text-gray-400 sm:py-6 dark:text-gray-400 dark:hover:text-gray-500" href="#">Products</Link>
              <Link to={'/about'} className="font-medium text-gray-500 hover:text-gray-400 sm:py-6 dark:text-gray-400 dark:hover:text-gray-500" href="#">About</Link>
              <Link to={'/contact'} className="font-medium text-gray-500 hover:text-gray-400 sm:py-6 dark:text-gray-400 dark:hover:text-gray-500" href="#">Contact</Link>
              
              <a className="flex items-center font-medium text-gray-500 hover:text-blue-600 sm:border-l sm:border-gray-300 sm:pl-6 dark:border-gray-700 dark:text-gray-400 dark:hover:text-blue-500" href="#">
                {user ?
                <>  
                  <Dropdown
                    align="right"
                    width="48"
                    trigger={
                      <button className="flex items-center text-sm focus:outline-none
                       bg-gray-800 text-white font-bold py-2 px-4 rounded transition duration-300 ease-in-out hover:bg-gray-900">
                        <div>{user?.name}</div>
                        <div className="ml-1">
                          <svg
                            className="fill-current h-4 w-4"
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 20 20">
                            <path
                              fillRule="evenodd"
                              // eslint-disable-next-line max-len
                              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                              clipRule="evenodd"
                            />
                          </svg>
                        </div>
                      </button>
                    }>
                    {/* Authentication */}
                      <Link to={'/product'} >
                        <DropdownButton >
                            MyProduct
                        </DropdownButton>
                      </Link>
                    <DropdownButton onClick={logout}>
                      Logout
                    </DropdownButton>
                  </Dropdown>
              </>
                :
                <>
                  <NavLink to="/login" className="bg-gray-800 text-white font-bold py-2 px-4 rounded transition duration-300 ease-in-out hover:bg-gray-900">
                    Login
                  </NavLink>
                </>
              }
              </a>
            </div>
          </div>
        </nav>
      </header>
              <div className="mt-20"></div>
    </>
  );

};
  
  export default Navbar;
