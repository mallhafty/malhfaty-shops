import axios from '../lib/axios'
import {useNavigate, } from 'react-router-dom';

export const Prodact = ({middleware, redirectIfAuthenticated} = {}) => {
  let navigate = useNavigate();

  const csrf = () => axios.get('/sanctum/csrf-cookie')

// --------------------------------------------------------------
  const addProduct = async ({ title, price, category, description, image, setErrors }) => {
    await csrf()
    setErrors([])
  
    const formData = new FormData()
    formData.append('title', title)
    formData.append('price', price)
    formData.append('category', category)
    formData.append('description', description)
    formData.append('image', image)
  
    axios.post('/api/myproducts', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
    .then(response => {
      console.log(response)
      navigate('/product')
    })
    .catch(error => {
      console.log(error.response.data)
      if (error.response.status !== 422) throw error
        setErrors(Object.values(error.response.data.errors).flat())
    })
  }



  return {
    addProduct,
  }
}
