import { Link } from 'react-router-dom';

function Home() {

  return (
    <>
       <>
      <section className="text-gray-600 body-font">
        <div className="container mx-auto flex px-5 py-24 md:flex-row flex-col items-center">
          <div className="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 className="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">
              Découvrez l'art
              <br className="hidden lg:inline-block" />
              du tissage saharien chez <span className="text-blue-600 text-bold text-4xl font-bold"> Mall<span className=" text-blue-600">hafty</span></span>
            </h1>
            <p className="mb-8 leading-relaxed">
              Mallhafty est une boutique dans une ligne spéciale de magasins de mode. Ce magasin en ligne propose une large gamme de vêtements traditionnels pour femmes, tous inspirés par la vie nomade au cœur du désert. Les tissus utilisés sont d'une qualité supérieure,            </p>
            <div className="flex justify-center">
              <Link to="/Products"><button className="inline-flex text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Product</button></Link>
              <Link to="/Contact"> <button className="ml-4 inline-flex text-gray-700 bg-gray-100 border-0 py-2 px-6 focus:outline-none hover:bg-gray-200 rounded text-lg">Contact</button></Link>
            </div>
          </div>
          <div className="lg:max-w-lg lg:w-full md:w-1/2 w-5/6">
            <img className="object-cover object-center rounded" alt="hero" src="/01.png" />
          </div>
        </div>
      </section>
    </>
    </>
  );
}

export default Home;
