const About = () => {
  return (
    <div className="min-h-screen">
    <div className="container mx-auto py-10 px-4 sm:px-6 lg:px-8">
      <h1 className="text-5xl text-center font-bold text-gray-800 mb-8">About Us</h1>
      <div className="flex flex-wrap -mx-4 mb-8 items-center">
        <div className="md:w-1/3 px-4 mb-4 md:mb-0">
          <img
            src="02.jpg"
            alt="Company Image"
            className="w-full h-full object-cover"
          />
        </div>
        <div className="md:w-2/3 px-4">
          <p className="text-gray-700 text-lg mb-1">
          Mallhafty est une boutique dans une ligne spéciale de magasins de mode. Ce magasin en ligne propose une large gamme de vêtements traditionnels pour femmes, tous inspirés par la vie nomade au cœur du désert. Les tissus utilisés sont d'une qualité supérieure, offrant un confort et une durabilité exceptionnels. Vous cherchiez les robes malahif et les confortables , vous pourrez essayer tous les cela et plus encore chez Mallhafty. De plus, grâce à votre service de livraison rapide et fiable, vous pouvez recevoir vos commandes directement de chez vous, car vous savez ce que vous faites. Alors n'hésitez plus et découvrez l'authentique mode saharienne avec Mallhafty !
          </p>
          
        </div>
      </div>
      
    </div>
  </div>
  );
};

export default About;
