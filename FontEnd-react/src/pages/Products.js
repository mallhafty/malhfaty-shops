import React from 'react'
import { Product } from '../components/export'
import Cart from '../components/navbar/Cart'

const Products = () => {
  return (
    <div className='mt-24'>
      <h1 className=" text-5xl  text-center font-bold mb-5">Products</h1>
      <Product />
      <div className="fixed bottom-10 right-10 bg-gray-900 hover:bg-gray-800 text-white font-semibold py-4 inline-flex items-center mr-2 px-4 rounded-full text-xl focus:outline-none shadow-2">
        <Cart />
      </div>
    </div>
  )
}

export default Products
