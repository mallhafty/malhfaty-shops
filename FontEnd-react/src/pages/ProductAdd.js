import { Link } from 'react-router-dom'
import AppLayout from '../components/components/Layouts/AppLayout'
import { useState } from 'react'
import { Prodact } from '../api/Product'
import AuthValidationErrors from '../components/components/AuthValidationErrors'

const ProductAdd = () => {
    const { addProduct } = Prodact({
        middleware: 'auth',
    })
    const [title , setTitle] = useState('')
    const [price , setPrice] = useState(0)
    const [category , setCategory] = useState('')
    const [description , setDescription] = useState('')
    const [image, setImage] = useState()
    const [errors, setErrors] = useState([])
    
    const [fileName, setFileName] = useState(null)
    const [file, setFile] = useState(null)


    const submitForm = (e) => {
        e.preventDefault()
        addProduct({image , title , price , category , description , setErrors})
    }

    const handleFileUpload = ({ target: {files} }) => {
        setImage(files[0])
        files[0] && setFileName(files[0].name)
        if (files) {
          setFile(URL.createObjectURL(files[0]))
        }
      }

    return (
        <AppLayout>
            <div>
                <div className="p-10 xs:p-0 mx-auto md:w-full md:max-w-xl"> 
                    <div className="bg-white shadow w-full rounded-lg divide-y divide-gray-200">
                        <div className="px-5 py-7">
                        <h1 className='text-4xl font-bold text-center mb-8 '>Add Products</h1>
                        <AuthValidationErrors className="mb-4" errors={errors} />
                            <form onSubmit={submitForm}>
                                <div className='mb-4'>
                                    <label htmlFor="title" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Title</label>
                                    <input type="text" value={title} onChange={e => setTitle(e.target.value)} id="title" placeholder="Title" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" required />
                                </div>
                                <div className="grid gap-6 mb-4 md:grid-cols-2">
                                    <div>
                                        <label htmlFor="price" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Price</label>
                                        <input type="number" value={price} onChange={e => setPrice(e.target.value)} id="price" placeholder="Price" className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 text-base outline-none text-gray-700 py-1 px-3 leading-8 transition-colors duration-200 ease-in-out" required />
                                    </div> 
                                    <div>
                                        <label htmlFor="phone" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Category</label>
                                        <select id="category" value={category} onChange={e => setCategory(e.target.value)} className="bg-gray-100 border border-gray-300 text-gray-900 text-sm rounded-lg focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 utline-none  w-full p-2.5   " required>
                                            <option value="">Select category </option>
                                            <option value="Mallahef">Mallahef</option>
                                            <option value="Robes">Robes</option>
                                            <option value="Sacs">Sacs</option>
                                            <option value="Montres">Montres</option>
                                            <option value="Chaussures">Chaussures</option>
                                        </select>
                                    </div>
                                </div>
                                <div className='mb-6'>
                                    <label htmlFor="message" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Description</label>
                                    <textarea id="message" value={description} onChange={e => setDescription(e.target.value)} placeholder="Description..." rows={4} className="w-full bg-gray-100 bg-opacity-50 rounded border border-gray-300 focus:border-indigo-500 focus:bg-white focus:ring-2 focus:ring-indigo-200 h-32 text-base outline-none text-gray-700 py-1 px-3 resize-none leading-6 transition-colors duration-200 ease-in-out" required />
                                </div>
                                <div className="flex items-center space-x-2 w-full mb-5">
                                    {
                                        file ? <img src={file} className='w-16 h-16 rounded-lg' />:null
                                    }
                                    <label htmlFor="dropzone-file" className="flex flex-col items-center justify-center w-16 h-16 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-bray-800  hover:bg-gray-100  ">
                                        <div className="flex flex-col items-center justify-center pt-5 pb-6">
                                            {!fileName ? "+" : '↺'}
                                        </div>
                                        <input id="dropzone-file" type="file" accept='image/*' className="hidden" onChange={handleFileUpload} />
                                    </label>
                                </div>
                                <div className='flex space-x-5 pt-2'>
                                    <Link to="/product" type="button" className="text-sm font-medium text-gray-900 rounded-lg border border-gray-200 py-2.5 w-full shadow-sm hover:shadow-sm text-center inline-block">
                                        <span className="inline-block mr-2">Annuler</span>
                                    </Link>
                                    <button type="submit" className="transition duration-200 bg-blue-600 hover:bg-blue-600 focus:bg-blue-700 focus:shadow-sm focus:ring-4 focus:ring-blue-500 focus:ring-opacity-50 text-white w-full py-2.5 rounded-lg text-sm shadow-sm hover:shadow-md font-semibold text-center inline-block">
                                        <span className="inline-block mr-2">Create product</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </AppLayout>
    )
}

export default ProductAdd
