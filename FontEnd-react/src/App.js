import { Route, Routes , BrowserRouter } from "react-router-dom";
import Navbar from './components/navbar/navbar'
import { Home, Login, Register, MyProducts, ProductAdd, NotFoundPage, About, Contact } from './pages/export';
import Products from "./pages/Products";

function App() {
  const routes = [
    { name: '*', element: <NotFoundPage /> },
    { name: '/', element: <Home /> },
    { name: '/product', element: <MyProducts /> },
    { name: '/products', element: <Products /> },
    { name: '/product/add', element: <ProductAdd /> },
    { name: '/login', element: <Login /> },
    { name: '/register', element: <Register /> },
    { name: '/about', element: <About /> },
    { name: '/contact', element: <Contact /> },
  ]
  return (
    <div className="flex flex-col h-screen">
      <BrowserRouter>
      <Navbar />
          <Routes>
            {routes.map((item , i) => (
              <Route key={i} path={item.name} element={item.element} />
            ))}
          </Routes>
        
      </BrowserRouter>
    </div>
  );
}

export default App;